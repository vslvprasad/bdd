package com.cucumber.test1;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.cc.myproject.EvenOdd;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class StepDefinition7 {

	EvenOdd e ;
	boolean isEven;
	@Given("^I Make IsEven Calls$")
	public void i_Make_IsEven_Calls() throws Throwable {
	
			System.out.println("i_Make_IsEven_Calls");
			e = new EvenOdd();
	}
	
	@Given("^I Send Data as:$")
	public void i_Send_Data_as(List<Integer> arg1) throws Throwable {

		System.out.println("???????????????????????????????????");

		arg1.stream().forEach(s->System.out.print(s));

		System.out.println("???????????????????????????????????");

	}


	@When("^I enter even numbers$")
	public void i_enter_even_numbers() throws Throwable {
		System.out.println("i_enter_even_numbers");
		isEven = e.isEven(2);
	}

@Then("^I verify even is printed on screens$")
public void i_verify_even_is_printed_on_screens() throws Throwable {

	System.out.println("i_verify_even_is_printed_on_screens");
	assertEquals(isEven, true);
}

@Then("^ThreeStars Displayed On Screens$")
public void threestars_Displayed_On_Screens() throws Throwable {
	System.out.println("threestars_Displayed_On_Screens");
	assertEquals("***", "***");
}


	@Before
	public void doSomethingBeforeStep(){
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");	
	}
	@After
	public void doSomethingAfter(Scenario scenario){
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	}
	
	//@AfterStep
	public void doSomethingBeforeStep(Scenario scenario){
	}
}
